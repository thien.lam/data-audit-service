package vn.datamart.dataauditservice.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import io.opentracing.contrib.spring.web.starter.WebTracingProperties;
import vn.datamart.config.GeneralConfig;

@EnableConfigurationProperties({ WebTracingProperties.class })
@Configuration
public class Config extends GeneralConfig {

}
