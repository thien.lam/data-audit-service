package vn.datamart.dataauditservice.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
	basePackages = "vn.datamart.dataauditservice.backend",
	entityManagerFactoryRef = "backendEntityManager",
	transactionManagerRef = "backendTransactionManager"
)
public class BackendDataSourceConfig {
	
	private static final String ROOT_PACKAGE_SCAN = "vn.datamart.dataauditservice.backend";

	@Autowired
	private HibernateProperty hibernateProperty;
	
	@Primary
	@Bean	
	@ConfigurationProperties(prefix = "spring.datasource")
	public DataSourceProperties backendDataSourceProperties() {
		return new DataSourceProperties();
	}
	
	@Primary
	@Bean
	@ConfigurationProperties(prefix = "spring.datasource.hikari")
	public DataSource backendDataSource() {		
		return backendDataSourceProperties().initializeDataSourceBuilder().type(HikariDataSource.class).build();
	}
	
	@Primary
	@Bean
	public LocalContainerEntityManagerFactoryBean backendEntityManager() {
		DataSource dataSource = backendDataSource();		
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(dataSource);
		em.setPackagesToScan(ROOT_PACKAGE_SCAN);
		em.setPersistenceUnitName("backend");
        
		HibernateJpaVendorAdapter vendor = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendor);
					  
	    em.setJpaProperties(hibernateProperty.getProperties());
		
		return em;
	}
	
	@Primary
	@Bean
	public JpaTransactionManager backendTransactionManager() {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(backendEntityManager().getObject());
		return transactionManager;
	}
}
