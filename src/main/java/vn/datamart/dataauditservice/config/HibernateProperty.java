package vn.datamart.dataauditservice.config;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class HibernateProperty {
	
	@Autowired
	private Environment env;	
	private Properties properties;
	
	public HibernateProperty() {
		
	}	
	
	public Properties getProperties() {
		if(this.properties == null) {
			this.properties = new Properties();
			this.properties.setProperty("hibernate.hbm2ddl.auto", env.getProperty("spring.jpa.hibernate.ddl-auto"));
			this.properties.setProperty("hibernate.dialect", env.getProperty("spring.jpa.properties.hibernate.dialect"));
			this.properties.setProperty("hibernate.show_sql", env.getProperty("spring.jpa.show-sql"));	
		}			
		return this.properties;
	}
}
