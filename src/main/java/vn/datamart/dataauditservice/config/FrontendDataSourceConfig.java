package vn.datamart.dataauditservice.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
	basePackages = "vn.datamart.dataauditservice.frontend",
	entityManagerFactoryRef = "frontendEntityManager",
	transactionManagerRef = "frontendTransactionManager"
)
public class FrontendDataSourceConfig {
	
	private static final String ROOT_PACKAGE_SCAN = "vn.datamart.dataauditservice.frontend";
	public static final String PERSISTENCE_UNIT_NAME = "frontend";
	public static final String FRONTEND_TRANSACTION_MANAGER = "frontendTransactionManager";
	public static final String ENTITY_MANAGER = "frontendEntityManager";
	
	@Autowired
	private HibernateProperty hibernateProperty;
	
	@Bean	
	@ConfigurationProperties(prefix = "spring.frontend.datasource")
	public DataSourceProperties frontendDataSourceProperties() {
		return new DataSourceProperties();
	}
	
	@Bean
	@ConfigurationProperties(prefix = "spring.frontend.datasource.hikari")
	public DataSource frontendDataSource() {		
		return frontendDataSourceProperties().initializeDataSourceBuilder().type(HikariDataSource.class).build();
	}
	
	@Bean
	public LocalContainerEntityManagerFactoryBean frontendEntityManager() {
		DataSource dataSource = frontendDataSource();		
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(dataSource);		
		em.setPersistenceUnitName(PERSISTENCE_UNIT_NAME);
		em.setPackagesToScan(ROOT_PACKAGE_SCAN);
        
		HibernateJpaVendorAdapter vendor = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendor);
					  
	    em.setJpaProperties(hibernateProperty.getProperties());
		
		return em;
	}
	
	@Bean
	public PlatformTransactionManager frontendTransactionManager() {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(frontendEntityManager().getObject());
		return transactionManager;
	}
}
