package vn.datamart.dataauditservice.constant;

public class ShopEventConstant {
	public static final String SHOP_EVENT_TABLE = "`shop_event`";
	public static final String SHOP_EVENT_ORDER_TABLE = "`shop_event_order`";
	public static final String SHOP_EVENT_LISTING_TABLE = "`shop_event_listing`";
	public static final String SHOP_EVENT_BACKUP_TABLE = "`shop_event_backup`";
	public static final String SHOP_EVENT_ORDER_BACKUP_TABLE = "`shop_event_order_backup`";
	public static final String SHOP_EVENT_LISTING_BACKUP_TABLE = "`shop_event_listing_backup`";
}
