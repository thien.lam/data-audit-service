package vn.datamart.dataauditservice.constant;

public class BusinessEventConstant {
	public static final String BUSINESS_EVENT_TABLE = "`business_event`";
	public static final String BUSINESS_EVENT_BACKUP_TABLE = "`business_event_backup`";
}
