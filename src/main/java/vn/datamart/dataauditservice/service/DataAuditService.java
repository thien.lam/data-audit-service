package vn.datamart.dataauditservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import vn.datamart.dataauditservice.backend.repository.DataAuditBeRepository;
import vn.datamart.dataauditservice.dto.DataAuditTable;
import vn.datamart.dataauditservice.frontend.repository.DataAuditFeRepository;
import vn.datamart.utils.StringUtils;
import vn.datamart.dataauditservice.config.FrontendDataSourceConfig;

@Service
public class DataAuditService {

	@Autowired
	private DataAuditBeRepository beRepository;

	@Autowired
	private DataAuditFeRepository feRepository;

	@Transactional(value = FrontendDataSourceConfig.FRONTEND_TRANSACTION_MANAGER)
	public void moveDataFe(String sourceTable, String destTable, String condition, int limit) {
		if(StringUtils.isEmpty(condition)) {
			return;
		}
		this.feRepository.moveData(sourceTable, destTable, condition, limit);
	}

	@Transactional(value = FrontendDataSourceConfig.FRONTEND_TRANSACTION_MANAGER, rollbackFor = Exception.class)
	public void moveDataFe(List<DataAuditTable> tables, int limit) {
		for(DataAuditTable table : tables) {
			this.feRepository.moveData(table.getSourceTable(), table.getDestTable(), table.getCondition(), limit);
		}
	}
	
	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
	public void moveDataBe(List<DataAuditTable> tables, int limit) {
		for (DataAuditTable able : tables) {
			this.beRepository.moveData(able.getSourceTable(), able.getDestTable(),
					able.getCondition(), limit);
		}
	}

	@Transactional(value = FrontendDataSourceConfig.FRONTEND_TRANSACTION_MANAGER, rollbackFor = RuntimeException.class)
	public void removeDataFe(String sourceTable, String condition) {
		if(StringUtils.isEmpty(condition)) {
			return;
		}
		this.feRepository.removeData(sourceTable, condition);
	}

	@Transactional(rollbackFor = RuntimeException.class)
	public void removeDataBe(String sourceTable, String condition) {
		this.beRepository.removeData(sourceTable, condition);
	}

}
