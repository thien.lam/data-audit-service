package vn.datamart.dataauditservice.dto;

public class DataAuditTable {
	private String sourceTable;
	private String destTable;
	private String condition;
	
	public DataAuditTable(String sourceTable, String destTable, String condition) {
		super();
		this.sourceTable = sourceTable;
		this.destTable = destTable;
		this.condition = condition;
	}

	public String getSourceTable() {
		return sourceTable;
	}

	public void setSourceTable(String sourceTable) {
		this.sourceTable = sourceTable;
	}

	public String getDestTable() {
		return destTable;
	}

	public void setDestTable(String destTable) {
		this.destTable = destTable;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

}
