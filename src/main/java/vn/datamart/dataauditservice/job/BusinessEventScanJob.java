package vn.datamart.dataauditservice.job;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.opentracing.Span;
import io.opentracing.Tracer;
import vn.datamart.dataauditservice.constant.BusinessEventConstant;
import vn.datamart.dataauditservice.frontend.businessevent.BusinessEventService;
import vn.datamart.dataauditservice.service.DataAuditService;
import vn.datamart.tracing.TracingUtils;
import vn.datamart.utils.CollectionUtils;
import vn.datamart.utils.LogUtils;

@Service
public class BusinessEventScanJob {

	private static final Logger logger = LoggerFactory.getLogger(BusinessEventScanJob.class);
	private static final String CLEAN_BUSINESS_EVENT = "clean_business_event";
	@Autowired
	private DataAuditService dataAuditService;

	@Autowired
	private BusinessEventService businessEventService;

	@Autowired
	private Tracer tracer;

	/***
	 * Scan business event followed by day and limit, move to table backup and clean business event
	 * @param days
	 * @param limit
	 */
	public void run(int days, int limit) {
		Span span = TracingUtils.generateSpan(this.tracer, CLEAN_BUSINESS_EVENT);
		try {
		long updatedAt = LocalDateTime.now().minusDays(days).toEpochSecond(ZoneOffset.UTC);
		List<Integer> businessEventIds = this.businessEventService.findIdsByDate(updatedAt, limit);
		
		if (CollectionUtils.isEmpty(businessEventIds)) {
			span.log("Do not find valid business event id");
			return;
		}
			StringBuilder condition = new StringBuilder();
			// condition: get id business event to move business event backup
			condition.append(" id IN (").append(getStringByIds(businessEventIds)).append(")");
			LogUtils.debug(logger, "business event id:" + businessEventIds);
			span.log("Move business event");
			this.dataAuditService.moveDataFe(BusinessEventConstant.BUSINESS_EVENT_TABLE,
					BusinessEventConstant.BUSINESS_EVENT_BACKUP_TABLE, condition.toString(), limit);
			this.dataAuditService.removeDataFe(BusinessEventConstant.BUSINESS_EVENT_TABLE, condition.toString());
		} catch (Exception e) {
			LogUtils.debug(logger, e.getMessage());
			span.log(e.getMessage());
			TracingUtils.logErrorSpan(span, e.getMessage());
			return;
		} finally {
			span.finish();
		}

	}

	private StringBuilder getStringByIds(List<Integer> id) {
		StringBuilder idBuilder = new StringBuilder();
		for (int i = 0; i < id.size(); i++) {
			idBuilder.append(id.get(i));
			if (i < id.size() - 1) {
				idBuilder.append(",");
			}
		}
		return idBuilder;
	}
}
