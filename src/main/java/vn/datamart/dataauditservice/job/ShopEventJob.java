package vn.datamart.dataauditservice.job;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.opentracing.Span;
import io.opentracing.Tracer;
import vn.datamart.dataauditservice.backend.shopevent.ShopEventService;
import vn.datamart.dataauditservice.backend.shopevent.listing.ShopEventListingService;
import vn.datamart.dataauditservice.backend.shopevent.listing.backup.ShopEventListingBackupService;
import vn.datamart.dataauditservice.backend.shopevent.order.ShopEventOrderService;
import vn.datamart.dataauditservice.backend.shopevent.order.backup.ShopEventOrderBackupService;
import vn.datamart.dataauditservice.constant.ShopEventConstant;
import vn.datamart.dataauditservice.dto.DataAuditTable;
import vn.datamart.dataauditservice.service.DataAuditService;
import vn.datamart.tracing.TracingUtils;
import vn.datamart.utils.CollectionUtils;
import vn.datamart.utils.LogUtils;

@Service
public class ShopEventJob {

	private static final Logger logger = LoggerFactory.getLogger(ShopEventJob.class);
	private static final String CLEAN_SHOP_EVENT = "clena_shop_event";

	@Autowired
	private ShopEventService shopEventService;

	@Autowired
	private ShopEventListingBackupService shopEventListingBackupService;

	@Autowired
	private ShopEventOrderBackupService shopEventOrderBackupService;

	@Autowired
	private ShopEventListingService shopEventListingService;

	@Autowired
	private ShopEventOrderService shopEventOrderService;

	@Autowired
	private DataAuditService dataAuditService;

	@Autowired
	private Tracer tracer;

	/***
	 * scan shop event followed by day and limit, then move data to table backup 
	 * and clean shop event table
	 * @param days
	 * @param limit
	 */
	public void run(int days, int limit) {
		Span span = TracingUtils.generateSpan(this.tracer, CLEAN_SHOP_EVENT);
		LocalDateTime updatedAt = LocalDateTime.now().minus(days, ChronoUnit.DAYS);
		List<Long> eventIds = this.shopEventService.findIdsByDate(updatedAt, limit);
		if (CollectionUtils.isEmpty(eventIds)) {
			StringBuilder message = new StringBuilder();
			message.append("Not found valid event id");
			span.log(message.toString());
			LogUtils.debug(logger, message.toString());
			span.finish();
			return;
		}
		StringBuilder condition = new StringBuilder();
		StringBuilder eventIdsBuilder = new StringBuilder();
		if (!CollectionUtils.isEmpty(eventIds)) {
			// condition: move shop event, shop event listing and order follow by event_id
			condition.append("event_id IN (");
			eventIdsBuilder = getStringBuilderByIds(eventIds);
			condition.append(eventIdsBuilder).append(")");
		}

		try {
			span.log("Move data of shop event");
			moveData(condition, limit);
		} catch (Exception e) {
			LogUtils.error(logger, e.getMessage(), e);
			TracingUtils.logErrorSpan(span, e.getMessage());
			span.finish();
			return;
		}

		// get event id which inserted in shop event listing and order back up
		List<Long> listingEventIds = this.shopEventListingBackupService.findInsertedEventIdsByEventIds(eventIds);
		List<Long> orderEventIds = this.shopEventOrderBackupService.findInsertedEventIdsByEventIds(eventIds);
		List<Long> migratedEventIds = new ArrayList<Long>();
		migratedEventIds.addAll(listingEventIds);
		migratedEventIds.addAll(orderEventIds);
		try {
			this.shopEventListingService.removeByEventIds(eventIdsBuilder.toString());
			this.shopEventOrderService.removeByEventIds(eventIdsBuilder.toString());
			if (CollectionUtils.isEmpty(migratedEventIds)) {
				this.shopEventService.removeById(eventIdsBuilder.toString());
			} else {
				this.shopEventService.removeById(getStringBuilderByIds(migratedEventIds).toString());
			}
		} catch (Exception e) {
			LogUtils.debug(logger, e.getMessage());
			TracingUtils.logErrorSpan(span, e.getMessage());
		} finally {
			span.finish();
		}

	}

	private void moveData(StringBuilder condition, int limit) {
		List<DataAuditTable> shopEventTables = new ArrayList<DataAuditTable>();
		// move data of source -> destination table with condition
		shopEventTables.add(new DataAuditTable(ShopEventConstant.SHOP_EVENT_ORDER_TABLE,
				ShopEventConstant.SHOP_EVENT_ORDER_BACKUP_TABLE, condition.toString()));

		shopEventTables.add(new DataAuditTable(ShopEventConstant.SHOP_EVENT_LISTING_TABLE,
				ShopEventConstant.SHOP_EVENT_LISTING_BACKUP_TABLE, condition.toString()));

		shopEventTables.add(
				new DataAuditTable(ShopEventConstant.SHOP_EVENT_TABLE, ShopEventConstant.SHOP_EVENT_BACKUP_TABLE, ""));
		this.dataAuditService.moveDataBe(shopEventTables, limit);
	}

	private StringBuilder getStringBuilderByIds(List<Long> id) {
		StringBuilder idBuilder = new StringBuilder();
		for (int i = 0; i < id.size(); i++) {
			idBuilder.append(id.get(i));
			if (i < id.size() - 1) {
				idBuilder.append(",");
			}
		}
		return idBuilder;
	}

}
