package vn.datamart.dataauditservice.job;

import java.util.Date;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.support.CronSequenceGenerator;

import vn.datamart.utils.LogUtils;

@Configuration
@EnableScheduling
@ConditionalOnProperty(value = "job.cron.enabled", havingValue = "true")
public class ScheduleScanJob {
	private static final Logger logger = LoggerFactory.getLogger(ScheduleScanJob.class);
	@Autowired
	private ShopEventJob shopEventjob;

	@Autowired
	private BusinessEventScanJob businessEventJob;
	
	@Value("${job.cron.expression}")
	private String cronExpression;
	@Value("${job.day}")
	private int dayCron;
	@Value("${job.limit}")
	private int limit;
	

	public ScheduleScanJob() {
	}

	@PostConstruct
	public void init() {
		CronSequenceGenerator cronTrigger = new CronSequenceGenerator(cronExpression);
		Date next = cronTrigger.next(new Date());
		LogUtils.info(logger, "First scan job  run at", next.toString());
	}

	/**
	 * Schedule and clean data of shop event table
	 */
	@Async
	@Scheduled(cron = "${job.cron.expression}")
	public void shopEventJob() {
		LogUtils.info(logger, "Schedule clean shop event scan job");
		this.shopEventjob.run(dayCron, limit);
	}

	/**
	 * Schedule and clean data of business event table
	 */
	@Async
	@Scheduled(cron = "${job.cron.expression}")
	public void businessEventJob() {
		LogUtils.info(logger, "Schedule clean business event scan job");
		this.businessEventJob.run(dayCron, limit);
	}
}
