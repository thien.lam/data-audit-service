package vn.datamart.dataauditservice.backend.shopevent.listing.backup;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.datamart.utils.CollectionUtils;

@Service
public class ShopEventListingBackupService {
	
	@Autowired
	private ShopEventListingBackupRepository repository;
	
	public List<Long> findInsertedEventIdsByEventIds(List<Long> eventIds){
		if(CollectionUtils.isEmpty(eventIds)) {
			return new ArrayList<Long>();
		}
		return this.repository.findInsertedEventIdsByEventIds(eventIds);
	}
	
	
}
