package vn.datamart.dataauditservice.backend.shopevent.order.backup;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ShopEventOrderBackupRepository extends CrudRepository<ShopEventOrderBackup, Long>{

	@Query("SELECT sob.eventId FROM ShopEventOrderBackup sob WHERE sob.eventId IN :eventIds")
	public List<Long> findInsertedEventIdsByEventIds(@Param("eventIds") List<Long> eventIds);
}
