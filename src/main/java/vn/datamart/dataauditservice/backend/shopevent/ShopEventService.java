package vn.datamart.dataauditservice.backend.shopevent;

import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.datamart.dataauditservice.constant.ShopEventConstant;
import vn.datamart.dataauditservice.service.DataAuditService;
import vn.datamart.utils.StringUtils;

@Service
public class ShopEventService {
	
	@Autowired
	private ShopEventRepository repository;
	
	@Autowired
	private DataAuditService dataAuditService;
	
	public List<Long> findIdsByDate(LocalDateTime updatedAt, int limit){
		return this.repository.findIdsByDate(updatedAt, limit);
	}
	
	public void removeById(String ids) {
		if(StringUtils.isEmpty(ids)) {
			return;
		}
		StringBuilder condition = new StringBuilder();
		condition.append("id IN (").append(ids).append(")");
		this.dataAuditService.removeDataBe(ShopEventConstant.SHOP_EVENT_TABLE, condition.toString());
	}
}
