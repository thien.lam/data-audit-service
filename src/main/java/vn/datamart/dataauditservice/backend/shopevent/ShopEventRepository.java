package vn.datamart.dataauditservice.backend.shopevent;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ShopEventRepository extends CrudRepository<ShopEvent, Long>{

	@Query(value = "SELECT id FROM shop_event WHERE updated_at <= :updatedAt LIMIT :limit", nativeQuery = true)
	public List<Long> findIdsByDate(@Param("updatedAt") LocalDateTime updatedAt,@Param("limit") int limit);
	
	@Query("DELETE FROM ShopEvent s WHERE s.id in :eventIds")
	public void deleteByIds(@Param("eventIds") List<Long> eventIds);
}
