package vn.datamart.dataauditservice.backend.shopevent.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.datamart.dataauditservice.constant.ShopEventConstant;
import vn.datamart.dataauditservice.service.DataAuditService;
import vn.datamart.utils.StringUtils;
@Service
public class ShopEventOrderService {
	
	@Autowired
	private DataAuditService dataAuditService;
	
	public void removeByEventIds(String eventIds) {
		if(StringUtils.isEmpty(eventIds)) {
			return;
		}
		StringBuilder condition = new StringBuilder();
		condition.append(" id IN (SELECT id FROM ").append(ShopEventConstant.SHOP_EVENT_ORDER_BACKUP_TABLE).append("WHERE ").append(" event_id IN (").append(eventIds).append("))");
		this.dataAuditService.removeDataBe(ShopEventConstant.SHOP_EVENT_ORDER_TABLE, condition.toString());
	}
}
