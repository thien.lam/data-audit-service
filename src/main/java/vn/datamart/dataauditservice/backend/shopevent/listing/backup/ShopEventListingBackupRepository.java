package vn.datamart.dataauditservice.backend.shopevent.listing.backup;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ShopEventListingBackupRepository extends CrudRepository<ShopEventListingBackup, Long> {
	@Query("SELECT selb.eventId FROM ShopEventListingBackup selb WHERE selb.eventId IN :eventIds")
	public List<Long> findInsertedEventIdsByEventIds(@Param("eventIds") List<Long> eventIds);
}
