package vn.datamart.dataauditservice.backend.shopevent;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "shop_event")
public class ShopEvent implements Serializable{
private static final long serialVersionUID = 1L;
	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id	
	private long id;
	private String event;
	
	@Column(name = "event_type")
	private String eventType;
	
	@Column(name = "is_served")
	private int isServed;
	
	@Column(name = "shop_id")
	private Long shopId;	
	
	@Column(name = "fe_shop_id")
	private int feShopId;
	
	@Column(name = "is_error")
	private int isError;
	
	private String info;
	
	@Column(name = "platform_id")
	private Integer platformId;
	
	@Column(name = "business_event_id")
	private Long businessEventId;

	@Column(name = "business_id")
	private Integer businessId;
	
	@Column(name = "created_by")
	private Integer createdBy;

	private String reason;
	
	@Column(name = "updated_at")
	private LocalDateTime updatedAt;
	
	public ShopEvent() {
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public int getIsServed() {
		return isServed;
	}

	public void setIsServed(int isServed) {
		this.isServed = isServed;
	}

	public Long getShopId() {
		return shopId;
	}

	public void setShopId(Long shopId) {
		this.shopId = shopId;
	}

	public int getFeShopId() {
		return feShopId;
	}

	public void setFeShopId(int feShopId) {
		this.feShopId = feShopId;
	}

	public int getIsError() {
		return isError;
	}

	public void setIsError(int isError) {
		this.isError = isError;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public Integer getPlatformId() {
		return platformId;
	}

	public void setPlatformId(Integer platformId) {
		this.platformId = platformId;
	}

	public Long getBusinessEventId() {
		return businessEventId;
	}

	public void setBusinessEventId(Long businessEventId) {
		this.businessEventId = businessEventId;
	}

	public Integer getBusinessId() {
		return businessId;
	}

	public void setBusinessId(Integer businessId) {
		this.businessId = businessId;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}
	
}
