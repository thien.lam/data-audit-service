package vn.datamart.dataauditservice.frontend.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import vn.datamart.dataauditservice.config.FrontendDataSourceConfig;
import vn.datamart.utils.StringUtils;

@Repository
public class DataAuditFeRepository {

	@PersistenceContext(unitName = FrontendDataSourceConfig.ENTITY_MANAGER)
	EntityManager entityManager;

	public void moveData(String sourceTable, String destTable, String condition, int limit) {
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT IGNORE INTO ").append(destTable).append(" SELECT * FROM ").append(sourceTable);
		if (!StringUtils.isEmpty(condition)) {
			sql.append("WHERE ").append(condition).append(" LIMIT :limit");
		} else {
			sql.append(" LIMIT :limit");
		}
		Query query = this.entityManager.createNativeQuery(sql.toString());
		query.setParameter("limit", limit);
		query.executeUpdate();
	}

	public void removeData(String sourceTable, String condition) {
		StringBuilder sql = new StringBuilder();
		sql.append("DELETE FROM ").append(sourceTable);
		if (!StringUtils.isEmpty(condition)) {
			sql.append("WHERE ").append(condition);
		}
		Query query = this.entityManager.createNativeQuery(sql.toString());
		query.executeUpdate();
	}
}
