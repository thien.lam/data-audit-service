package vn.datamart.dataauditservice.frontend.businessevent;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BusinessEventService {

	@Autowired
	private BusinessEventRepository repository;
	
	public List<Integer> findIdsByDate(long updatedAt, int limit){
		return this.repository.findIdsByDate(updatedAt, limit);
	}
}
