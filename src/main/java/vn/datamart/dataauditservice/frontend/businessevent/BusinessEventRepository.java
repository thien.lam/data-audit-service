package vn.datamart.dataauditservice.frontend.businessevent;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface BusinessEventRepository extends CrudRepository<BusinessEvent, Long> {

	@Query(value = "SELECT id FROM business_event WHERE updated_at <= :updatedAt LIMIT :limit", nativeQuery = true)
	List<Integer> findIdsByDate(@Param("updatedAt") long updatedAt, @Param("limit") int limit);
}
