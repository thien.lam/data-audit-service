package vn.datamart.dataauditservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataAuditServiceApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(DataAuditServiceApplication.class, args);
	}
}
